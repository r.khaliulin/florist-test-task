<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 12:57
 */

namespace App\Interfaces;

/**
 * Interface ConfigInterface
 * @package App\Interfaces
 */
interface ConfigInterface
{

    /**
     * Получаем массив параметров сервера
     * @param string $server
     * @return array|null
     */
    public function getServerParams($server);

    /**
     * Получаем отдельно взятый параметр
     * @param string $server
     * @param string $param
     * @return mixed
     */
    public function getServerParam($server, $param);

    /**
     * Устанавливаем отдельно взятый параметр
     * @param string $server
     * @param string $param
     * @param mixed $value
     */
    public function setServerParam($server, $param, $value);

    /**
     * Получаем основной сервер
     * @return string
     */
    public function getMasterServer();

    /**
     * Задаем основной сервер
     * @param string $masterServer
     */
    public function setMasterServer($masterServer);

}