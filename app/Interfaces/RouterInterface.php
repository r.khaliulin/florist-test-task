<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 10:21
 */

namespace App\Interfaces;

/**
 * Interface RouterInterface
 * @package App\Interfaces
 */
interface RouterInterface
{

    /**
     * @return array
     */
    public function getCommands();

    /**
     * @return mixed
     */
    public function getParam($param);

    /**
     * @return array
     */
    public function getParams();

    /**
     * @return mixed
     */
    public function getServer();

}