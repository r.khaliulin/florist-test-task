<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 26.09.2018
 * Time: 6:37
 */

namespace App\Interfaces;

/**
 * Interface IDataSourceInterface
 * @package App\Interfaces
 */
interface IDataSourceInterface
{

    /**
     * Метод для добавления данных
     * @param string $dataTable абстрактная таблица куда заносим данные
     * @param array $params параметры которые заносим в строку
     * @return mixed
     */
    public function insert($dataTable, $params);

    /**
     * Метод для считывания данных
     * @param string $dataTable абстрактная таблица куда заносим данные
     * @param mixed $byField по какому полю ищем строку
     * @param mixed $byValue значение поля
     * @return mixed
     */
    public function read($dataTable, $whereField = null, $whereValue = null);

    /**
     * Обновляем запись
     * @param string $dataTable
     * @param string $setField
     * @param mixed $setValue
     * @param string $whereField
     * @param mixed $whereValue
     * @return bool
     */
    public function update($dataTable, $setField, $setValue, $whereField, $whereValue);

}