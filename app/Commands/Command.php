<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 9:53
 */

namespace App\Commands;

use App\Interfaces\ConfigInterface;
use App\Interfaces\IDataSourceInterface;
use App\Interfaces\RouterInterface;


/**
 * Class Command
 * @package App\Commands
 */
abstract class Command
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var IDataSourceInterface
     */
    private $masterDataSource;

    /**
     * @var IDataSourceInterface
     */
    private $localDataSource;

    /**
     * Command constructor.
     * @param RouterInterface $router
     * @param ConfigInterface $config
     * @param IDataSourceInterface $masterDataSource
     * @param IDataSourceInterface $localDataSource
     */
    public function __construct(
        RouterInterface $router,
        ConfigInterface $config,
        IDataSourceInterface $masterDataSource,
        IDataSourceInterface $localDataSource
    ) {
        $this->router = $router;
        $this->config = $config;
        $this->masterDataSource = $masterDataSource;
        $this->localDataSource = $localDataSource;
    }

    /**
     * @return RouterInterface
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * @return ConfigInterface
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return IDataSourceInterface
     */
    public function getMasterDataSource()
    {
        return $this->masterDataSource;
    }

    /**
     * @return IDataSourceInterface
     */
    public function getLocalDataSource()
    {
        return $this->localDataSource;
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return mb_strtolower((new \ReflectionClass(static::class))->getShortName());
    }

    /**
     * Создаем экземпляр с внедрением нужных объектов
     * @param RouterInterface $router
     * @param ConfigInterface $config
     * @return static
     */
    public static function create(RouterInterface $router, ConfigInterface $config) {
        // Получаем название класса объекта для работы с данными
        $localDataSourceClass = $config->getServerParam($router->getServer(), 'data_source');
        // Получаем конфигурацию
        $localDataSourceConfig = $config->getServerParam($router->getServer(), 'data_source_config');
        // Получаем название класса объекта для работы с данными
        $masterDataSourceClass = $config->getServerParam($config->getMasterServer(), 'data_source');
        // Получаем конфигурацию
        $masterDataSourceConfig = $config->getServerParam($config->getMasterServer(), 'data_source_config');
        return new static(
            $router,
            $config,
            $masterDataSourceClass::create(new $masterDataSourceConfig($config->getMasterServer())),
            $localDataSourceClass::create(new $localDataSourceConfig($router->getServer()))
        );
    }

}