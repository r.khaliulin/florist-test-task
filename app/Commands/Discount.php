<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 17:35
 */

namespace App\Commands;

/**
 * Class Discount
 * @package App\Commands
 */
class Discount extends Command
{

    /**
     * Создаем дисконтную карту
     */
    public function createDiscount()
    {
        $result = $this->getMasterDataSource()->insert(
            $this->getTableName(),
            [
                'server' => strval($this->getRouter()->getServer()),
                'discount' => intval($this->getRouter()->getParam('discount')),
                'num' => intval($this->getRouter()->getParam('num')),
                'sum' => 0
            ]
        );
        if ($result) {
            print("Карта успешно создана!\n");
            return;
        }
        print("Произошла ошибка при создании карты!");
    }

    /**
     * Получаем скидку по карте
     */
    public function getDiscount()
    {
        $discountCardNumber = $this->getRouter()->getParam('num');
        if ($discountCardNumber) {
            $discountCard = $this->getMasterDataSource()->read(
                $this->getTableName(),
                'num',
                $this->getRouter()->getParam('num')
            );
            if ($discountCard) {
                $discount = $this->calculate($discountCard);
                print("Скидка по карте №{$discountCardNumber}: {$discount}%. Сумма покупок по карте: {$discountCard['sum']}\n");
                return;
            }
            print("Информация по карте {$discountCardNumber} не найдена!\n");
            return;
        }
        print("Укажите номер карты при помощи параметра --num=номер_карты\n");
    }

    /**
     * Получаем карточку
     * @param $discountCardNum
     * @return array|bool
     */
    public function getCard($discountCardNum) {
        return $this->getMasterDataSource()->read($this->getTableName(), 'num', $discountCardNum);
    }

    /**
     * Производим расчет скидки у карты
     * @param array $discountCard
     * @return int
     */
    public function calculate($discountCard)
    {
        // Смотрим, существует ли альтернативная формула дискаунта для этого сервера
        $altFormula = $this->getConfig()->getServerParam($this->getRouter()->getServer(), 'discount_rule');
        if ($altFormula) {
            // Рассчитываем и возвращаем, если есть
            $discount = $altFormula($discountCard['sum']);
            print "Значение скидки успешно рассчитано по альтернативной формуле: {$discount}%\n";
            return $discount;
        }
        // Если нет, возвращаем оригинальное значение
        return $discountCard['discount'];
    }

    /**
     * Добавляем сумму покупок к карточке
     * @param array $discountCard
     * @return int
     */
    public function increaseSum($discountCard, $sum)
    {
        $newSum = $discountCard['sum'] + $sum;
        print "К карте {$discountCard['num']} была добавлена сумма {$sum}. Общий счет {$newSum}.\n";
        return $this->getMasterDataSource()->update($this->getTableName(), 'sum', $newSum, 'num', $discountCard['num']);
    }

}