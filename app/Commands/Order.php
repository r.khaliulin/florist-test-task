<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 9:53
 */

namespace App\Commands;

/**
 * Class Order
 * @package App\Commands
 */
class Order extends Command
{

    /**
     * Создать заказ
     */
    public function createOrder()
    {
        $discount = 0;
        $total = floatval($this->getRouter()->getParam('total'));
        $discountCardNum = intval($this->getRouter()->getParam('discount_card'));
        if (!empty($discountCardNum)) {
            // Создаем модель для работы со скидками
            $discountObj = Discount::create($this->getRouter(), $this->getConfig());
            // Подтягиваем карточку, если она существует
            if ($discountCard = $discountObj->getCard($discountCardNum)) {
                // Пересчитываем скидку
                $discount = $discountObj->calculate($discountCard);
                // Рассчитываем новую стоимость
                $total = $total - ($discount * ($total / 100));
                // Добавляем её к общей сумме покупок
                $discountObj->increaseSum($discountCard, $total);
            } else {
                print("Дисконтной карты {$discountCardNum} не существует! Попробуйте создать заказ с действующей картой либо уберите параметр --discount_card.\n");
                return;
            }
        }
        $result = $this->getMasterDataSource()->insert(
            $this->getTableName(),
            [
                'server' => strval($this->getRouter()->getServer()),
                'item' => strval($this->getRouter()->getParam('item')),
                'amount' => intval($this->getRouter()->getParam('amount')),
                'total' => $total,
                'discount_val' => $discount,
                'param' => strval($this->getRouter()->getParam('param'))
            ]
        );
        if ($result) {
            print("Заказ успешно добавлен!\n");
            return;
        }
        print("При добавлении заказа произошла ошибка!");
    }

    /**
     * Читаем и выводим данные о всех заказах
     */
    public function listOrders()
    {
        $allOrders = $this->getMasterDataSource()->read($this->getTableName());
        if (!empty($allOrders)) {
            $mask = "|%-10s |%-13s |%-12s |%-12s |%-13s |%-12s |\n";
            printf($mask, 'server', 'item', 'amount', 'total', 'discount_val', 'param');
            foreach ($allOrders as $order) {
                printf($mask, $order['server'], $order['item'], $order['amount'], $order['total'], $order['discount_val'], $order['param']);
            }
        }
    }

}