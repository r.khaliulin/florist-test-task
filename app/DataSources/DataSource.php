<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 13:30
 */

namespace App\DataSources;

use App\Interfaces\IDataSourceConfigInterface;
use App\Interfaces\IDataSourceInterface;

/**
 * Class Source
 * @package App\DataSources
 */
abstract class DataSource implements IDataSourceInterface
{

    /**
     * @param string $dataTable
     * @param array $params
     * @return mixed
     */
    abstract public function insert($dataTable, $params);

    /**
     * @param string $dataTable
     * @param null $byField
     * @param null $byValue
     * @return mixed
     */
    abstract public function read($dataTable, $byField = null, $byValue = null);

    /**
     * Обновляем запись
     * @param string $dataTable
     * @param string $setField
     * @param mixed $setValue
     * @param string $whereField
     * @param mixed $whereValue
     * @return bool
     */
    abstract function update($dataTable, $setField, $setValue, $whereField, $whereValue);

    /**
     * @param IDataSourceConfigInterface $config
     * @return static
     */
    public static function create(IDataSourceConfigInterface $config) {
        return new static($config);
    }

}