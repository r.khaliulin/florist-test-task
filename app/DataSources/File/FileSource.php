<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 26.09.2018
 * Time: 6:36
 */

namespace App\DataSources\File;

use App\DataSources\DataSource;
use App\Interfaces\IDataSourceConfigInterface;

/**
    TODO: Данный класс был сделан исключительно для тестирования и ни на что не претендует.
 */
class FileSource extends DataSource
{

    /**
     * @var IDataSourceConfigInterface
     */
    private $config;

    /**
     * @var array
     */
    private $database;

    /**
     * FileSource constructor.
     * @param IDataSourceConfigInterface $config
     */
    public function __construct(IDataSourceConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * Пишем строку
     * @param string $dataTable
     * @param array $params
     * @return bool|int
     */
    public function insert($dataTable, $params)
    {
        $this->load();
        $id = (!empty($this->database[$dataTable]) ? count($this->database[$dataTable]) : 0) + 1;
        $this->database[$dataTable][$id] = array_merge(['id' => $id], $params);
        if ($this->save()) {
            return count($this->database[$dataTable]);
        }
        return false;
    }

    /**
     * @param string $dataTable
     * @param null $whereField
     * @param null $whereValue
     * @return bool|mixed
     */
    public function read($dataTable, $whereField = null, $whereValue = null)
    {
        $this->load();
        if (!empty($this->database[$dataTable])) {
            if (empty($whereField) || empty($whereValue)) {
                return $this->database[$dataTable];
            }
            if ($whereField == 'id') {
                return $this->database[$dataTable][$whereValue] ? $this->database[$dataTable][$whereValue] : false;
            }
            foreach ($this->database[$dataTable] as $row) {
                if (isset($row[$whereField]) && $row[$whereField] == $whereValue) {
                    return $row;
                }
            }
        }
        return false;
    }

    /**
     * Обновляем запись
     * @param $dataTable
     * @param $setField
     * @param $setValue
     * @param $whereField
     * @param $whereValue
     * @return bool
     */
    public function update($dataTable, $setField, $setValue, $whereField, $whereValue)
    {
        $this->load();
        if (!empty($this->database[$dataTable])) {
            foreach ($this->database[$dataTable] as $key => $row) {
                if (isset($row[$whereField]) && $row[$whereField] == $whereValue) {
                    $this->database[$dataTable][$key][$setField] = $setValue;
                    return $this->save();
                }
            }
        }
        return false;
    }

    /**
     * Загружаем базу
     */
    private function load()
    {
        $content = json_decode(@file_get_contents($this->config->getDbPath()), true);
        $this->database = $content ? $content : [];
    }

    /**
     * Сохраняем базу
     */
    private function save()
    {
        return file_put_contents($this->config->getDbPath(), json_encode($this->database));
    }

}