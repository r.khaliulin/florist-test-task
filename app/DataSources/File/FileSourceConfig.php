<?php
/**
 * Created by PhpStorm.
 * User: hedin
 * Date: 26.09.2018
 * Time: 13:14
 */

namespace App\DataSources\File;

use App\Interfaces\IDataSourceConfigInterface;

/**
 * Class FileSourceConfig
 * @package App\DataSources\File
 */
class FileSourceConfig implements IDataSourceConfigInterface
{

    /**
     * Путь до файла с данными
     * @var string
     */
    private $dbPath;

    /**
     * Путь до директории с данными
     * @var string
     */
    private $dataDir;

    /**
     * Название базы
     * @var string
     */
    private $dbName;

    /**
     * FileSourceConfig constructor.
     */
    public function __construct($dbName)
    {
        $this->setDbName($dbName);
        $this->setDataDir(realpath(getenv('HOME')) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR);
        $this->setDbPath($this->dataDir . $dbName . '.json');
    }

    /**
     * @return string
     */
    public function getDataDir()
    {
        return $this->dataDir;
    }

    /**
     * @param string $dataDir
     */
    public function setDataDir($dataDir)
    {
        @mkdir($dataDir, 0777, true);
        $this->dataDir = $dataDir;
    }

    /**
     * @return string
     */
    public function getDbPath()
    {
        return $this->dbPath;
    }

    /**
     * @param string $dbPath
     */
    public function setDbPath($dbPath)
    {
        $this->dbPath = $dbPath;
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param mixed $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

}