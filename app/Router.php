<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 26.09.2018
 * Time: 3:27
 */

namespace App;

use App\Interfaces\RouterInterface;

/**
 * Class Router
 * @package app
 */
class Router implements RouterInterface
{

    /**
     * Максимальное количество параметров
     */
    const MAX_ARGV = 1000;

    /**
     * Сервер
     * @var string
     */
    private $server = '';

    /**
     * Команды
     * @var array
     */
    private $commands = [];

    /**
     * Параметры
     * @var array
     */
    private $params = [];

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->parseRoute($GLOBALS['argv']);
    }

    /**
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param string $command
     * @return bool
     */
    public function commandExist($command)
    {
        return isset($this->commands[$command]);
    }

    /**
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $param
     * @return mixed
     */
    public function getParam($param) {
        return isset($this->params[$param]) ? $this->params[$param] : null;
    }

    /**
     * Создаем экземпляр
     * @return Router
     */
    public static function create() {
        return new self;
    }

    /**
     * Метод для добавления и обработки маршрута
     * @param string $command
     * @param string $callback
     * @return mixed
     */
    public function route($command, $callback)
    {
        // Проверяем есть ли такая команда
        if ($this->commandExist($command)) {
            $callback = explode('@', $callback);
            // Вызываем нужный метод
            if (!empty($callback[0]) && !empty($callback[1])) {
                return $callback[0]::create($this, Config::create())->{$callback[1]}();
            }
        }
        return false;
    }

    /**
     * Распарсиваем входные параметры
     * @param $argv
     */
    private function parseRoute($argv)
    {
        // Сервер
        if (preg_match('/([^_]+)?_?([^_]+)?\./', $argv[0], $matches) === 1) {
            if (!empty($matches[2])) {
                $this->server = $matches[2];
            }
        }
        $index = 1;
        // Остальные параметры
        while ($index < self::MAX_ARGV && isset($argv[$index])) {
            if (preg_match('/^([^-\=]+.*)$/', $argv[$index], $matches) === 1) {
                $this->commands[$matches[1]] = true;
            } else if (preg_match('/^-+(.+)$/', $argv[$index], $matches) === 1) {
                if (preg_match('/^-+(.+)\=(.+)$/', $argv[$index], $subMatches) === 1) {
                    $this->params[$subMatches[1]] = $subMatches[2];
                } else if (isset($argv[$index + 1]) && preg_match('/^[^-\=]+$/', $argv[$index + 1]) === 1) {
                    $this->params[$matches[1]] = $argv[$index + 1];
                    $index++;
                } else {
                    $this->params[$matches[1]] = true;
                }
            }
            $index++;
        }
    }

}