<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 26.09.2018
 * Time: 6:19
 */

namespace App;

use App\Interfaces\ConfigInterface;

/**
 * Class Config
 * @package App
 */
class Config implements ConfigInterface
{

    /**
     * Название основного сервера
     * @var string
     */
    private $masterServer = '';

    /**
     * Параметры серверов
     * @var array
     */
    private $serverParams = [];

    /**
     * Инициализируем и присваиваем все параметры
     */
    public function configure()
    {
        // Устанавливаем основной сервер
        $this->setMasterServer('main');
        // Конфигурируем серверы
        $this->setServerParams([
            'main' => [
                // Класс отвечающий за работу с данными
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig',
                // Альтернативные правила для расчета скидки
                'discount_rule' => function($cardTotal) {
                    $discount = 0;
                    if ($cardTotal < 3000) {
                        $discount = 10;
                    } else if ($cardTotal >= 3000 && $cardTotal < 6000) {
                        $discount = 15;
                    } else if ($cardTotal >= 6000) {
                        $discount = 20;
                    }
                    return $discount;
                }
            ],
            'white' => [
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig',
                // Альтернативные правила для расчета скидки
                'discount_rule' => function($cardTotal) {
                    $discount = 0;
                    if ($cardTotal < 3000) {
                        $discount = 10;
                    } else if ($cardTotal >= 3000 && $cardTotal < 6000) {
                        $discount = 15;
                    } else if ($cardTotal >= 6000) {
                        $discount = 20;
                    }
                    return $discount;
                }
            ],
            'pink' => [
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig',
                // Альтернативные правила для расчета скидки
                'discount_rule' => function($cardTotal) {
                    $discount = 0;
                    if ($cardTotal < 2000) {
                        $discount = 5;
                    } else if ($cardTotal >= 2000 && $cardTotal < 5000) {
                        $discount = 7;
                    } else if ($cardTotal >= 5000) {
                        $discount = 10;
                    }
                    return $discount;
                }
            ],
            'pink1' => [
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig',
                // Альтернативные правила для расчета скидки
                'discount_rule' => function($cardTotal) {
                    $discount = 0;
                    if ($cardTotal < 2000) {
                        $discount = 5;
                    } else if ($cardTotal >= 2000 && $cardTotal < 5000) {
                        $discount = 7;
                    } else if ($cardTotal >= 5000) {
                        $discount = 10;
                    }
                    return $discount;
                }
            ],
            'pink2' => [
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig',
                // Альтернативные правила для расчета скидки
                'discount_rule' => function($cardTotal) {
                    $discount = 0;
                    if ($cardTotal < 2000) {
                        $discount = 5;
                    } else if ($cardTotal >= 2000 && $cardTotal < 5000) {
                        $discount = 7;
                    } else if ($cardTotal >= 5000) {
                        $discount = 10;
                    }
                    return $discount;
                }
            ],
            'blue' => [
                'data_source' => 'App\DataSources\File\FileSource',
                'data_source_config' => 'App\DataSources\File\FileSourceConfig'
            ]
        ]);
    }

    /**
     * Получаем массив параметров сервера
     * @param string $server
     * @return array|null
     */
    public function getServerParams($server)
    {
        return isset($this->serverParams[$server]) ? $this->serverParams[$server] : null;
    }

    /**
     * Устанавливаем параметры сервера
     * @param array $serverParams
     */
    public function setServerParams($serverParams)
    {
        $this->serverParams = $serverParams;
    }

    /**
     * Получаем отдельно взятый параметр
     * @param string $server
     * @param string $param
     * @return mixed
     */
    public function getServerParam($server, $param)
    {
        return isset($this->serverParams[$server][$param]) ? $this->serverParams[$server][$param] : null;
    }

    /**
     * Устанавливаем отдельно взятый параметр
     * @param string $server
     * @param string $param
     * @param mixed $value
     */
    public function setServerParam($server, $param, $value)
    {
        $this->serverParams[$server][$param] = $value;
    }

    /**
     * Получаем основной сервер
     * @return string
     */
    public function getMasterServer()
    {
        return $this->masterServer;
    }

    /**
     * Задаем основной сервер
     * @param string $masterServer
     */
    public function setMasterServer($masterServer)
    {
        $this->masterServer = $masterServer;
    }

    /**
     * Создаем экземпляр
     * @return Config
     */
    public static function create() {
        $config = new self;
        $config->configure();
        return $config;
    }

}