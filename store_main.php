<?php

// Выполняем автозагрузку
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';
});

// Создаем роутер
$router = \App\Router::create();
// Команда на создание заказа
$router->route('create_order', 'App\Commands\Order@createOrder');
// Команда для вывода списка заказов
$router->route('list_order', 'App\Commands\Order@listOrders');
// Команда для создания дисконтной карты
$router->route('create_card', 'App\Commands\Discount@createDiscount');
// Команда для применения дисконтной карты к заказу
$router->route('get_discount', 'App\Commands\Discount@getDiscount');